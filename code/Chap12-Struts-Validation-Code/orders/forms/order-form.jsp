<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD><TITLE>No More Divets</TITLE></HEAD>
<BODY BGCOLOR="#FDF5E6">
<H1 ALIGN="CENTER">No More Divets</H1>
Thanks for ordering the Fred Randall
Amazing Un-Divet for the bargain-basement
price of $199.95. To complete your
order, please fill out and submit the
following information.
<P>
<CENTER>
<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean"
           prefix="bean" %>
<UL>
<html:messages id="msg" message="false">
  <LI><B><FONT COLOR="RED">
    <bean:write name="msg"/>
  </FONT></B></LI>
</html:messages>
</UL>
<html:form action="/actions/order"
           onsubmit="return validateOrderFormBean(this);">
  First name: <html:text property="firstName"/><BR>
  Last name: <html:text property="lastName"/><BR>
  Mailing address: <html:text property="address"/><BR>
  ZIP Code: <html:text property="zipCode"/><BR>
  Credit Card Number:
  <html:text property="creditCardNumber"/><BR>
  Email address for confirmation:
  <html:text property="email"/><BR>
  <html:submit value="Order Now!"/>
</html:form>
<html:javascript formName="orderFormBean"/>
</CENTER>
</BODY></HTML>