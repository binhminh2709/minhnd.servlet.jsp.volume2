package coreservlets;

import javax.servlet.http.*;
import org.apache.struts.action.*;

/** Bean with fields for formatting resume. The bean provides
 *  a validate method for checking the input fields. The Struts
 *  system calls the validate method after populating the fields.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class FormatFormBean extends ActionForm {
  private String titleSize = "";
  private String headingSize = "";
  private String bodySize = "";
  private String bgColor = "";
  private String fgColor = "";

  public String getTitleSize() {
    return(titleSize);
  }

  public void setTitleSize(String titleSize) {
    this.titleSize = titleSize;
  }

  public String getHeadingSize() {
    return(headingSize);
  }

  public void setHeadingSize(String headingSize) {
    this.headingSize = headingSize;
  }

  public String getBodySize() {
    return(bodySize);
  }

  public void setBodySize(String bodySize) {
    this.bodySize = bodySize;
  }

  public String getBgColor() {
    return(bgColor);
  }

  public void setBgColor(String bgColor) {
    this.bgColor = bgColor;
  }

  public String getFgColor() {
    return(fgColor);
  }

  public void setFgColor(String fgColor) {
    this.fgColor = fgColor;
  }

  public String getStyleSheet() {
    return(
      "<STYLE TYPE=\"text/css\">\n" +
      "<!--\n" +
      "H1 { font-size: " + titleSize + "px; }\n" +
      "H2 { font-size: " + headingSize + "px; }\n" +
      "BODY { font-size: " + bodySize + "px;\n" +
      "       background-color: " + bgColor + ";\n" +
      "       color: " + fgColor + "; }\n" +
      "-->\n" +
      "</STYLE>");
    }

  public ActionErrors validate(ActionMapping mapping,
                               HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    if (isMissing(getTitleSize())) {
      errors.add("title",
                 new ActionMessage("titleSize.required"));
    }
    if (isMissing(getHeadingSize())) {
      errors.add("heading",
                 new ActionMessage("headingSize.required"));
    }
    if (isMissing(getBodySize())) {
      errors.add("body",
                 new ActionMessage("bodySize.required"));
    }
    if (isMissing(getBgColor())) {
      errors.add("bg",
                 new ActionMessage("bgColor.required"));
    }
    if (isMissing(getFgColor())) {
      errors.add("fg",
                 new ActionMessage("fgColor.required"));
    } else if (getFgColor().equals(getBgColor())) {
      errors.add("fg",
                 new ActionMessage("colors.notMatch"));
    }
    return(errors);
  }

  private boolean isMissing(String value) {
      return((value == null) || (value.trim().equals("")));
  }
}
