package coreservlets;

import javax.servlet.http.*;
import org.apache.struts.action.*;

/** Bean with fields for formatting resume. Other than
 *  isMissing method, the bean provides no error checking
 *  of the input fields.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class FormatFormBean extends ActionForm {
  private String titleSize = "";
  private String headingSize = "";
  private String bodySize = "";
  private String bgColor = "";
  private String fgColor = "";

  public String getTitleSize() {
    return(titleSize);
  }

  public void setTitleSize(String titleSize) {
    this.titleSize = titleSize;
  }

  public String getHeadingSize() {
    return(headingSize);
  }

  public void setHeadingSize(String headingSize) {
    this.headingSize = headingSize;
  }

  public String getBodySize() {
    return(bodySize);
  }

  public void setBodySize(String bodySize) {
    this.bodySize = bodySize;
  }

  public String getBgColor() {
    return(bgColor);
  }

  public void setBgColor(String bgColor) {
    this.bgColor = bgColor;
  }

  public String getFgColor() {
    return(fgColor);
  }

  public void setFgColor(String fgColor) {
    this.fgColor = fgColor;
  }

  public String getStyleSheet() {
    return(
      "<STYLE TYPE=\"text/css\">\n" +
      "<!--\n" +
      "H1 { font-size: " + titleSize + "px; }\n" +
      "H2 { font-size: " + headingSize + "px; }\n" +
      "BODY { font-size: " + bodySize + "px;\n" +
      "       background-color: " + bgColor + ";\n" +
      "       color: " + fgColor + "; }\n" +
      "-->\n" +
      "</STYLE>");
    }

  public boolean isMissing(String value) {
    return((value == null) || (value.trim().equals("")));
  }
}
