package coreservlets;

/** This class simulates the retrieval of world records
 *  from the FINA database.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class WorldRecords {
  public static Object[][] getRecentRecords() {
    Object[][] records = {
      { "Event", "Name", "Time" },
      { "400 IM", "Michael Phelps", "4:08.25"},
      { "100 Br", "Lindsay Hall", "1:05.08"},
      { "200 IM", "Katie Hoff", "2:09.71"}};
    return(records);
  }
}