package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** This servlet creates 2 arrays and stores them in the 
 *  request scope as attributes and forwards the request
 *  to the loop-test.jsp page.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class LoopTest extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    String[] servers =
      {"Tomcat", "Resin", "JRun", "WebLogic",
       "WebSphere", "Oracle 10g", "JBoss" };
    request.setAttribute("servers", servers);
    Object[][] records = WorldRecords.getRecentRecords();
    request.setAttribute("records", records);
    String address = "/WEB-INF/results/loop-test.jsp";
    RequestDispatcher dispatcher =
      request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
