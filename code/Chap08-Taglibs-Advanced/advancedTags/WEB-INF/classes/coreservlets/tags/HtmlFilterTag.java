package coreservlets.tags;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;
import coreservlets.ServletUtilities;

/** Tag that replaces special HTML characters (like less than
 *  and greater than signs) with their HTML character entities.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class HtmlFilterTag extends SimpleTagSupport {
  public void doTag() throws JspException, IOException {
    // Buffer tag body's output
    StringWriter stringWriter = new StringWriter();
    getJspBody().invoke(stringWriter);

    // Filter out any special HTML characters
    // (e.g., "<" becomes "&lt;")
    String output =
      ServletUtilities.filter(stringWriter.toString());

    // Send output to the client
    JspWriter out = getJspContext().getOut();
    out.print(output);
  }
}
