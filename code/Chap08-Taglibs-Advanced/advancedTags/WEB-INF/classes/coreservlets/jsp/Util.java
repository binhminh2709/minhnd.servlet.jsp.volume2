package coreservlets.jsp;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

/** Utility class whose method is used as an JSP EL function.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class Util {
  public static String information(HttpServletRequest request) {
    String result = "";
    result += "Agent Header: " + request.getHeader("User-Agent");
    result += "<BR>";
    result += "Parameters:<BR>";
    Enumeration paramNames = request.getParameterNames();
    while (paramNames.hasMoreElements()) {
       String paramName = (String) paramNames.nextElement();
       result += paramName + "<BR>";
    }
    return result;
  }
}
