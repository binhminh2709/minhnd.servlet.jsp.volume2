<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>There's one of these born every minute...</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Thanks for Ordering
  </TABLE>

  <H2>Your Purchases</H2>
  <jsp:useBean id="items" class="coreservlets.ItemList" scope="session" />
  <jsp:getProperty name="items" property="itemTable" />

  <%-- Note the lack of "boats" at the front of URI below --%>
  <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
  <boats:count />
</BODY>
</HTML>