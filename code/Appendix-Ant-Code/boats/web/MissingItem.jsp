<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>Missing Item Number</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Missing Item Number
  </TABLE>
  <P>
  <H2>Error</H2>
  <SPAN CLASS="ERROR">You must supply an item number!</SPAN>

  <%-- Note the lack of "boats" at the front of URI below --%>
  <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
  <boats:count />
</BODY>
</HTML>