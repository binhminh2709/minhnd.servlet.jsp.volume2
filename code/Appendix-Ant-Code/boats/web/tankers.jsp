<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>Oil Tankers</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Oil Tankers
  </TABLE>
  <P>
    Stable and roomy models for the <S>uninformed</S> innovative buyer.
  <H2>Available Models</H2>
  Choose a model to see a picture along with price and availability information.

  <FORM ACTION="DisplayItem">
    <INPUT TYPE="RADIO" NAME="itemNum" VALUE="Valdez"> Valdez -- Slightly damaged model available at discount<BR> <INPUT TYPE="RADIO"
      NAME="itemNum" VALUE="BigBertha"> Big Bertha -- Includes 10 million gallon swimming pool<BR> <INPUT TYPE="RADIO" NAME="itemNum"
      VALUE="EcoDisaster"> ED I -- For those who don't mind political incorrectness
    <P>
    <CENTER>
      <INPUT TYPE="SUBMIT" VALUE="Get Details">
    </CENTER>
  </FORM>

  <%-- Note the lack of "boats" at the front of URI below --%>
  <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
  <boats:count />
</BODY>
</HTML>