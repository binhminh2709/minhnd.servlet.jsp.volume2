package coreservlets;

import java.util.Date;

/** Simple bean used by CounterTag. For boats example.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class Count {
  private int count = 1;
  private Date startDate = new Date();

  public int getCount() {
    return(count);
  }

  public void incrementCount() {
    count++;
  }

  public Date getStartDate() {
    return(startDate);
  }
}
