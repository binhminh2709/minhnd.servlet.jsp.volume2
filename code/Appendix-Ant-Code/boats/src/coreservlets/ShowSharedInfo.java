package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

/** Summarizes information on cookies, sessions,
 *  and the servlet context. Illustrates that sessions
 *  and the servlet context are separate for each Web app.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ShowSharedInfo extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    PrintWriter out = response.getWriter();
    String title = "Shared Info";
    out.println(ServletUtilities.headWithTitle(title) +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1 ALIGN=\"CENTER\">" + title + "</H1>\n" +
                "<UL>\n" +
                "  <LI>Cookies:<UL>");
    Cookie[] cookies = request.getCookies();
    if ((cookies == null) || (cookies.length == 0)) {
      out.println("    <LI>No cookies found.");
    } else {
      Cookie cookie;
      for(int i=0; i<cookies.length; i++) {
        cookie = cookies[i];
        out.println("    <LI>" + cookie.getName());
      }
    }
    out.println("    </UL>\n" +
                "  <LI>Session:<UL>");
    HttpSession session = request.getSession(true);
    Enumeration attributes = session.getAttributeNames();
    if (!attributes.hasMoreElements()) {
        out.println("    <LI>No attributes found.");
    } else {
      while(attributes.hasMoreElements()) {
        out.println("    <LI>" + attributes.nextElement());
      }
    }
    out.println("    </UL>\n" +
                "  <LI>Servlet Context:<UL>");
    ServletContext application = getServletContext();
    attributes = application.getAttributeNames();
    if (!attributes.hasMoreElements()) {
        out.println("    <LI>No attributes found.");
    } else {
      while(attributes.hasMoreElements()) {
        out.println("    <LI>" + attributes.nextElement());
      }
    }
    out.println("    </UL>\n" +
                "</UL>\n" +
                "</BODY></HTML>");
  }
}
