package coreservlets;

import java.util.*;

/** Very simple pseudo shopping cart. Maintains a list
 *  of items and can format them in an HTML table.
 *  Used in the boats Web app example to show that
 *  each Web app maintains its own set of sessions.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ItemList {
  private ArrayList items = new ArrayList();

  public synchronized void setNewItem(SimpleItem newItem) {
    if (newItem != null) {
      items.add(newItem);
    }
  }

  public synchronized String getItemTable() {
    if (items.size() == 0) {
      return("<H3>No items...</H3>");
    }
    String tableString =
      "<TABLE BORDER=1>\n" +
      "  <TR CLASS=\"COLORED\">\n" +
      "      <TH>Item Number\n" +
      "      <TH>Description\n" +
      "      <TH>Cost\n";
    for(int i=0; i<items.size(); i++) {
      SimpleItem item = (SimpleItem)items.get(i);
      tableString +=
        "  <TR><TD>" + item.getItemNum() + "\n" +
        "      <TD>" + item.getDescription() + "\n" +
        "      <TD>" + item.getCostString() + "\n";
    }
    tableString += "</TABLE>";
    return(tableString);
  }

  public synchronized String toString() {
    return("[Item List: " + items.size() + " entries.]");
  }
}
