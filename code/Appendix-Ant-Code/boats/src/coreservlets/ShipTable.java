package coreservlets;

/** A small collection of ships. Used in the boats Web app.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ShipTable {
  private static SimpleItem[] ships =
  { // Yachts
    new SimpleItem
      ("BM1",
       "Base model yacht. Features sauna, two kitchens, " +
         "and four-car garage. <B>Perfect entry-level " +
         "yacht for the first-time buyer</B>.",
       "images/yacht.jpg",
       72678922.99),
    new SimpleItem
      ("MR1",
       "Mid-range yacht. Features helipad, bowling alley, " +
         "and 15 bedrooms. <B>Trade up from a BM1 " +
         "today!</B>.",
       "images/yacht.jpg",
       145357845.98),
    new SimpleItem
      ("HE1",
       "High-end yacht. Features onboard 18-hole golf " +
         "course, onboard polo grounds, and ski jump. " +
         "<B>Bonus: for a limited time only, a mid-sized " +
         "tropical island country will be included at " +
         "no extra cost.</B>",
       "images/yacht.jpg",
       7267892299.00),
    // Oil Tankers
    new SimpleItem
      ("Valdez",
       "Slightly damaged former Alaskan touring boat. " +
         "<B>Special price won't last long!</B>",
       "images/tanker.jpg",
       9.95),
    new SimpleItem
      ("BigBertha",
       "Tired of cramped quarters on your boat? " +
         "This roomy model has plenty of space to stretch " +
         "your legs. <B>10 million gallon onboard " +
         "swimming pool included!</B>",
       "images/tanker.jpg",
       20000000.00),
    new SimpleItem
      ("EcoDisaster",
       "OK, ok, so this model is not exactly politically " +
         "correct. <B>But you're not one to pass up " +
         "a bargain just because of a few " +
         "<S>Greenpeace</S> pesky demonstrators, " +
         "are you?</B>.",
       "images/tanker.jpg",
       100000000),
    // Aircraft Carriers
    new SimpleItem
      ("SafeT-1A",
       "A safe and secure boat, perfect for family " +
         "vacations. <B>Note:</B> no crew provided. If crew " +
         "is desired, please see model SafeT-1B.",
       "images/carrier.jpg",
       3167492481.99),
     new SimpleItem
      ("SafeT-1B",
       "Just like the 1A model, but we provide the crew. " +
         "<B>Note:</B> You must pay the one million dollar " +
         "annual salary for the crew.",
       "images/carrier.jpg",
       3267492481.99),
    new SimpleItem
      ("Lubber-1",
       "All the comfort of the other models, but without " +
         "the danger. Realistic simulation provides " +
         "continuous water sounds. <B>Note:</B> " +
         "currently located in Siberia. Shipping and " +
         "handling not included.",
       "images/carrier.jpg",
       152.99)
  };

  private static ItemTable shipTable =
    new ItemTable(ships);

  public static ItemTable getShipTable() {
    return(shipTable);
  }
}

