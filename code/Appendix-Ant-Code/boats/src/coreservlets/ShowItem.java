package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Servlet that looks up information on an item that is for
 *  sale. Uses the MVC architecture, with either
 *  MissingItem.jsp or ShowItem.jsp doing the presentation.
 *  Used in the boats Web app.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ShowItem extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    String itemNum = request.getParameter("itemNum");
    String destination;
    if (itemNum == null) {
      destination = "/MissingItem.jsp";
    } else {
      destination = "/ShowItem.jsp";
      ItemTable shipTable = ShipTable.getShipTable();
      SimpleItem item = shipTable.getItem(itemNum);
      request.setAttribute("item", item);
    }
    RequestDispatcher dispatcher =
      getServletContext().getRequestDispatcher(destination);
    dispatcher.forward(request, response);
  }
}
