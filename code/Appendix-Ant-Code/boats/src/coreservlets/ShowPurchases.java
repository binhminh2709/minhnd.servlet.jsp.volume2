package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** A simple servlet that shows a table of purchases.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class ShowPurchases extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    String itemNum = request.getParameter("itemNum");
    ItemTable shipTable = ShipTable.getShipTable();
    SimpleItem item = shipTable.getItem(itemNum);
    HttpSession session = request.getSession(true);
    ItemList previousItems =
      (ItemList)session.getAttribute("items");
    if (previousItems == null) {
      previousItems = new ItemList();
      session.setAttribute("items", previousItems);
    }
    previousItems.setNewItem(item);
    RequestDispatcher dispatcher =
      getServletContext().getRequestDispatcher("/sucker.jsp");
    dispatcher.forward(request, response);
  }
}
