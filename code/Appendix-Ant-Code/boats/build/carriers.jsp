<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<TITLE>Aircraft Carriers</TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Aircraft Carriers
  </TABLE>
  <P>
    High-security models for the <S>paranoid</S> careful buyer.
  <H2>Available Models</H2>
  Choose a model to see a picture along with price and availability information.

  <FORM ACTION="DisplayItem">
    <INPUT TYPE="RADIO" NAME="itemNum" VALUE="SafeT-1A"> SafeT-1A -- Our Most Popular Model<BR> <INPUT TYPE="RADIO" NAME="itemNum"
      VALUE="SafeT-1B"> SafeT-1B -- 1000-man crew included<BR> <INPUT TYPE="RADIO" NAME="itemNum" VALUE="Lubber-1"> Land Lubber I
    -- Land-based replica; no water to worry about!
    <P>
    <CENTER>
      <INPUT TYPE="SUBMIT" VALUE="Get Details">
    </CENTER>
  </FORM>

  <%-- Note the lack of "boats" at the front of URI below --%>
  <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
  <boats:count />
</BODY>
</HTML>