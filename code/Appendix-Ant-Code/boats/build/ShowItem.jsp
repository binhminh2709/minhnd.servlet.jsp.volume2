<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD>
<jsp:useBean id="item" class="coreservlets.SimpleItem" scope="request" />
<TITLE><jsp:getProperty name="item" property="itemNum" /></TITLE>
<LINK REL=STYLESHEET HREF="app-styles.css" TYPE="text/css">
</HEAD>

<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE"><jsp:getProperty name="item" property="itemNum" />
  </TABLE>
  <P>
    <IMG SRC="<jsp:getProperty name='item' property='imageURL' />" ALIGN="RIGHT">
  <H3>
    Item Number
    </H2>
    <jsp:getProperty name="item" property="itemNum" />

    <H3>
      Description
      </H2>
      <jsp:getProperty name="item" property="description" />

      <H3>
        Cost
        </H2>
        <jsp:getProperty name="item" property="costString" />. A real bargain!

        <H3>
          Ordering
          </H2>
          <FORM ACTION="DisplayPurchases">
            <INPUT TYPE="HIDDEN" NAME="itemNum" VALUE="<jsp:getProperty name='item'
                                 property='itemNum' />">
            <INPUT TYPE="SUBMIT" VALUE="Submit Order">
          </FORM>

          <%-- Note the lack of "boats" at the front of URI below --%>
          <%@ taglib uri="/WEB-INF/tlds/count-taglib.tld" prefix="boats"%>
          <boats:count />
</BODY>
</HTML>