
package JFCBook.Chapter10;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import JFCBook.Chapter10.FileTree;

public class FileTreeTest {
  
  public static void main(String[] args) {
    final FileTree ft;
    try {
      if (args.length < 2) {
        System.out.println("Usage java FileTreeTest directory title [/F]");
        System.exit(0);
      }
      //JFrame f = new JFrame(args[0].substring(1,args[0].lastIndexOf("\\")));
      JFrame f = new JFrame(args[1]);
      if (args.length == 3 && "/F".equals(args[2])) {
        ft = new FileTree(args[0], true);
      } else {
        ft = new FileTree(args[0], false);
      }
      
      ft.addTreeSelectionListener(new TreeSelectionListener() {
        
        public void valueChanged(TreeSelectionEvent evt) {
          TreePath path = evt.getPath();
          String name = ft.getPathName(path);
          File file = ft.getFile(path);
          System.out.println("File " + name + " has been " + (evt.isAddedPath() ? "selected" : "deselected"));
          System.out.println("File object is " + file);
        }
      });
      
      f.getContentPane().add(new JScrollPane(ft));
      f.setSize(300, 300);
      f.addWindowListener(new WindowAdapter() {
        
        public void windowClosing(WindowEvent evt) {
          System.exit(0);
        }
      });
      f.setVisible(true);
    } catch (FileNotFoundException e) {
      System.out.println("File " + args[0] + " not found");
    }
  }
}
