package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** This servlet is used to demonstrate forwarding to a
 *  protected resource.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class SecurityHoleServlet extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    RequestDispatcher dispatcher = 
    	request.getRequestDispatcher("/secure/job-openings.html");
    dispatcher.forward(request, response);
  }

  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }
}
