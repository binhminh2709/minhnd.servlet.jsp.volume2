package coreservlets.filters;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** This filter converts any request that it is configured to
 *  intercept into a direct client request. This prevents
 *  developers from making a mistake by dynamically forwarding
 *  the client to a secure resource, bypassing the 
 *  declarative security mechanism.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class SecurityHolePlugFilter implements Filter {
  public void doFilter(ServletRequest request,
                       ServletResponse response,
                       FilterChain chain)
      throws ServletException, IOException {
	HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    res.sendRedirect(req.getRequestURI());
  }

  public void init(FilterConfig config)
      throws ServletException {
  }
  
  public void destroy() {}
}
