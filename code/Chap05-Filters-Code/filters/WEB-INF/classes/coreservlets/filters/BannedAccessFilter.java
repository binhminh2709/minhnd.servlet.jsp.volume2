package coreservlets.filters;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Filter that refuses access to anyone connecting directly
 *  from or following a link from a banned site.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class BannedAccessFilter implements Filter {
  private HashSet<String> bannedSiteTable;

  /** Deny access if the request comes from a banned site
   *  or is referred here by a banned site.
   */
  public void doFilter(ServletRequest request,
                       ServletResponse response,
                       FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest req = (HttpServletRequest)request;
    String requestingHost = req.getRemoteHost();
    String referringHost =
      getReferringHost(req.getHeader("Referer"));
    String bannedSite = null;
    boolean isBanned = false;
    if (bannedSiteTable.contains(requestingHost)) {
      bannedSite = requestingHost;
      isBanned = true;
    } else if (bannedSiteTable.contains(referringHost)) {
      bannedSite = referringHost;
      isBanned = true;
    }
    if (isBanned) {
      showWarning(response, bannedSite);
    } else {
      chain.doFilter(request,response);
    }
  }

  /** Create a table of banned sites based on initialization
   *  parameters .
   */
  public void init(FilterConfig config)
      throws ServletException {
    bannedSiteTable = new HashSet<String>();
    String bannedSites =
      config.getInitParameter("bannedSites");
    if (bannedSites == null) {
    	return;
    }
    // Split using one or more white spaces
    String[] sites = bannedSites.split("\\s++");
    for (String bannedSite: sites) {
    	bannedSiteTable.add(bannedSite);
    	System.out.println("Banned " + bannedSite);
    }
  }
  
  public void destroy() {}

  private String getReferringHost(String refererringURLString) {
    try {
      URL referringURL = new URL(refererringURLString);
      return(referringURL.getHost());
    } catch(MalformedURLException mue) { // Malformed or null
      return(null);
    }
  }

  // Replacement response that is returned to users
  // who are from or referred here by a banned site.

  private void showWarning(ServletResponse response,
                           String bannedSite)
      throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
      "Transitional//EN\">\n";
    out.println
      (docType +
       "<HTML>\n" +
       "<HEAD><TITLE>Access Prohibited</TITLE></HEAD>\n" +
       "<BODY BGCOLOR=\"WHITE\">\n" +
       "<H1>Access Prohibited</H1>\n" +
       "Sorry, access from or via " + bannedSite + "\n" +
       "is not allowed.\n" +
       "</BODY></HTML>");
  }
}
