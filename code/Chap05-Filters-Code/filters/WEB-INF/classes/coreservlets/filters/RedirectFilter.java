package coreservlets.filters;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Snippet used in "Blocking Access" section.
 *  Not a complete filter!
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class RedirectFilter implements Filter {
  public void doFilter(ServletRequest request,
                       ServletResponse response,
                       FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    if (isUnusualCondition(req)) {
      res.sendRedirect("http://www.somesite.com");
    } else {
      chain.doFilter(req,res);
    }
  }

  public boolean isUnusualCondition(HttpServletRequest req) {
    return(true);
  }

  public void init(FilterConfig config)
      throws ServletException {
  }
  
  public void destroy() {}
}
