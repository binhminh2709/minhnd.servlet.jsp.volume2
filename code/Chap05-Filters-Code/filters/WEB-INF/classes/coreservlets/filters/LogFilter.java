package coreservlets.filters;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Simple filter that prints a report in the log file
 *  whenever the associated servlets or JSP pages
 *  are accessed.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class LogFilter implements Filter {
  protected FilterConfig config;
  private ServletContext context;
  private String filterName;
  
  public void doFilter(ServletRequest request,
                       ServletResponse response,
                       FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest req = (HttpServletRequest)request;
    context.log(req.getRemoteHost() +
                " tried to access " +
                req.getRequestURL() +
                " on " + new Date() + ". " +
                "(Reported by " + filterName + ".)");
    chain.doFilter(request,response);
  }

  public void init(FilterConfig config)
      throws ServletException {
    this.config = config; // In case it is needed by subclass.
    context = config.getServletContext();
    filterName = config.getFilterName();
  }
  
  public void destroy() {}
}
