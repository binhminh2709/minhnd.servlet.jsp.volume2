package coreservlets.filters;

/** Filter that replaces all occurrences of the target
 *  string with the replacement string. The target and 
 *  replacement strings are provided as init parameters
 *  to the filter in the web.xml file.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class ReplaceSiteNameFilter extends ModificationFilter {
	private boolean isCaseInsensitive = false;
	
  /** The string that needs replacement.
   */
  public String getTarget() {
  	return getInitParameter("target");
  }

  /** The string that replaces the target.
   */
  public String getReplacement() {
  	return getInitParameter("replacement");
  }
  
  /** Returns the init parameter value specified by 'param' or 
   *  null if it is not present or an empty string
   */
  private String getInitParameter(String param) {
  	String value = config.getInitParameter(param);
  	if ((value == null) || (value.trim().equals(""))) {
  		value = null;
  	}
  	
  	return value;
  }
  
  /** Sets whether the search for the target string
   *  will be case sensitive.
   */
  public void setCaseInsensitive(boolean flag) {
    isCaseInsensitive = flag;
  }

  /** Returns true or false, indicating if the search
   *  for the target string is case sensitive.
   */
  public boolean isCaseInsensitive() {
    return(isCaseInsensitive);
  }

  /** Replaces all strings matching the target string
   *  with the replacement string.
   */
  public String doModification(String orig) {
    if ((getTarget() == null) || (getReplacement() == null)) {
      return(orig);
    } else {
      String target = getTarget();
      if (isCaseInsensitive()) {
        target = "(?i)" + target;
      }
      String replacement = getReplacement();
      return(orig.replaceAll(target, replacement));
    }
  }
}
