<%@ taglib uri="http://struts.apache.org/tags-bean"
           prefix="bean" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD><TITLE>
<bean:message key="form.title"/>:
<bean:message key="form.successString"/>
</TITLE></HEAD>
<BODY BGCOLOR="#FDF5E6"><CENTER>
<H1>
<bean:message key="form.title"/>:
<bean:message key="form.successString"/>
</H1>
<UL>
  <LI><bean:message key="form.firstName"/>:
      <bean:write name="registrationBean"
                  property="firstName"/>
  <LI><bean:message key="form.lastName"/>:
      <bean:write name="registrationBean"
                  property="lastName"/>
  <LI><bean:message key="form.emailAddress"/>:
      <bean:write name="registrationBean"
                  property="emailAddress"/>
</UL>
</CENTER>
</BODY></HTML>