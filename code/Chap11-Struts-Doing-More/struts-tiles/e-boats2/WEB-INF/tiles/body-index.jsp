<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<P>
Looking for a hole in the water into which to pour your money?
You've come to the right place! We offer a wide selection of
reasonably priced boats for everyday use.
<IMG SRC="<html:rewrite page='/images/yacht.jpg'/>"
     WIDTH=240 HEIGHT=367
     ALIGN="RIGHT" ALT="Base-model yacht">

<H2>Yachts</H2>
Starting at a mere 72 million, these entry-level models are
perfect for the cost-conscious buyer.
Click <A HREF="<html:rewrite page='/yachts.jsp'/>">
here</A> for details.

<H2>Oil Tankers</H2>
Looking for something a bit bigger and sturdier? These
roomy models come complete with large swimming pools.
Click <A HREF="<html:rewrite page='/tankers.jsp'/>">
here</A> for details.

<H2>Aircraft Carriers</H2>
Concerned about security? These high-tech models come
equipped with the latest anti-theft devices.
Click <A HREF="<html:rewrite page='/carriers.jsp'/>">
here</A> for details.