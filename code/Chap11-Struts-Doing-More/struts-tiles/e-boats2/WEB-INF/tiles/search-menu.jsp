<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<CENTER>
<TABLE BORDER=1>
  <TR BGCOLOR="BLACK"><TH>
    <FONT COLOR="WHITE">Search Site</FONT>
  </TH></TR>
  <TR><TH>
    <FORM ACTION=
        "<html:rewrite page='/actions/siteSearch.do'/>">
      <INPUT TYPE="TEXT" NAME="query"><BR>
      <INPUT TYPE="SUBMIT" VALUE="Search">
    </FORM>
  </TH></TR>
</TABLE>
<P>
<TABLE BORDER=1>
  <TR BGCOLOR="BLACK">
    <TH><FONT COLOR="WHITE">Search Web</FONT></TH>
  </TR>
  <TR><TH>
    <FORM ACTION="http://google.com/search">
      <INPUT TYPE="HIDDEN" NAME="hl" VALUE="en">
      <INPUT TYPE="TEXT" NAME="q"><BR>
      <INPUT TYPE="SUBMIT" VALUE="Search">
    </FORM>
  </TH></TR>
</TABLE>
</CENTER>