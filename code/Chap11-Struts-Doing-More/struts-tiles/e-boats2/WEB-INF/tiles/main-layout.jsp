<%@ taglib uri="http://struts.apache.org/tags-tiles"
           prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<HTML>
<HEAD><TITLE><tiles:getAsString name="title"/></TITLE>
<LINK REL="STYLESHEET"
      HREF="<html:rewrite page='/css/styles.css'/>"
      TYPE="text/css">
</HEAD>
<BODY>
<tiles:insert attribute="header"/>
<P>
<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE"><tiles:getAsString name="title"/>
</TH></TR></TABLE>
<P>
<TABLE WIDTH=75 ALIGN="LEFT" CELLSPACING="5">
<TR><TD><tiles:insert attribute="search-menu"/></TD></TR>
</TABLE>
<tiles:insert attribute="body"/>
<BR CLEAR="ALL">
<HR>
<tiles:insert attribute="footer"/>
</BODY></HTML>