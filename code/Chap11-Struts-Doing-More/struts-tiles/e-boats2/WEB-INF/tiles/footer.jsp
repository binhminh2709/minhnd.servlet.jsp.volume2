<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<CENTER>
<A HREF="<html:rewrite page='/index.html'/>">Home</A>
<A HREF="<html:rewrite page='/contact.html'/>">Contact</A>
<A HREF="<html:rewrite page='/privacy.html'/>">Privacy</A>
</CENTER>