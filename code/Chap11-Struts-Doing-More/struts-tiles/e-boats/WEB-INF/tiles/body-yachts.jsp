<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
Luxurious models for the <S>wasteful</S>
wealthy buyer.
<H2>Available Models</H2>
Choose a model to see a picture along with price and
availability information.
<FORM ACTION="<html:rewrite action='/actions/displayItem'/>">
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="BM1">
Base Model -- Includes 4-car garage<BR>
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="MR1">
Mid Range -- Has 15 bedrooms and a helipad<BR>
<INPUT TYPE="RADIO" NAME="itemNum" VALUE="HE1">
High End -- Free tropical island nation included
<P>
<CENTER>
<INPUT TYPE="SUBMIT" VALUE="Get Details">
</CENTER>
</FORM>
<CENTER>
<IMG SRC="<html:rewrite page='/images/yacht.jpg'/>"
     ALT="Yacht"></CENTER>