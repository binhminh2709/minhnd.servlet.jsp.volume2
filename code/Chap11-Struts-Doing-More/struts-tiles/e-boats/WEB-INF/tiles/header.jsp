<%@ taglib uri="http://struts.apache.org/tags-html"
           prefix="html" %>
<!--
Taken from Core Servlets and JavaServer Pages Volume II
from Prentice Hall and Sun Microsystems Press,
http://volume2.coreservlets.com/.
(C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
may be freely used or adapted.
-->
<TABLE WIDTH="100%" CLASS="DARK">
  <TR>
    <TH ALIGN="LEFT">
      <A HREF="<html:rewrite page='/index.html'/>"
         CLASS="WHITE">
      Home</A>&nbsp;&nbsp;&nbsp;
      <A HREF="<html:rewrite page='/products.html'/>"
         CLASS="WHITE">
      Products</A>&nbsp;&nbsp;&nbsp;
      <A HREF="<html:rewrite page='/services.html'/>"
         CLASS="WHITE">
      Services</A>&nbsp;&nbsp;&nbsp;
      <A HREF="<html:rewrite page='/contact.html'/>"
         CLASS="WHITE">
      Contact Us</A>
    </TH>
    <TH ALIGN="RIGHT">
      <A HREF="<html:rewrite action='/actions/showCart'/>"
         CLASS="WHITE">
      My Cart</A>&nbsp;&nbsp;&nbsp;
      <A HREF="<html:rewrite action='/actions/logout'/>"
         CLASS="WHITE">
      Logout</A>&nbsp;&nbsp;&nbsp;
      <A HREF="<html:rewrite page='/help.html'/>"
         CLASS="WHITE">
      Help</A>
    </TH>
  </TR>
</TABLE>