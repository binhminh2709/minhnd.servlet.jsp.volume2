<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Request Frequency Statistics</TITLE>
<LINK REL=STYLESHEET
      HREF="events-styles.css"
      TYPE="text/css">
</HEAD>
<BODY>
<TABLE BORDER=5 ALIGN="CENTER">
  <TR><TH CLASS="TITLE">Request Frequency Statistics</TABLE>
<P>
<UL>
<LI>Total number of requests in the life of this
    Web application: ${stats.totalRequests}.
<LI>Web application has been up for
    ${stats.totalSeconds} seconds.
<LI>This means that our request load is about
    ${stats.ratio} requests per second.
</UL>
</BODY>
</HTML>