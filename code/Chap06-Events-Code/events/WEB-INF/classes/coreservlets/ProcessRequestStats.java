package coreservlets;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/** Servlet that calculates request frequency as the number
 *  of requests made per second and forwards those results
 *  to a page that displays them.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class ProcessRequestStats extends HttpServlet {

  protected void doGet(HttpServletRequest request, 
                       HttpServletResponse response) 
        throws ServletException, IOException {
    // Retrieve requestRecords
    ServletContext context = this.getServletContext();
    ArrayList<Long> requestRecords = 
      (ArrayList<Long>) context.getAttribute("requestRecords");
    long firstMillis = requestRecords.get(0);
    int totalRequests = requestRecords.size();
    long lastMillis = requestRecords.get(totalRequests - 1);
    // Calculate total seconds elapsed
    long totalSeconds = (lastMillis - firstMillis) / 1000;
    // Calculate ration of requests per second
    double ratio = totalRequests / (double) totalSeconds;
    ratio = ((int)(ratio * 100) / (double) 100);

    // Populate RequestStats bean; store it in the request scope and
    // forward it to stats page
    RequestStatsBean requestStats = 
      new RequestStatsBean(totalSeconds, totalRequests, ratio);
    request.setAttribute("stats", requestStats);
    RequestDispatcher dispatcher = 
      request.getRequestDispatcher("/WEB-INF/pages/req-stats.jsp");
    dispatcher.forward(request, response);
  }
}
