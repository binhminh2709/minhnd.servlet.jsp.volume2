package coreservlets;

/** Bean used to store the collected request frequency data.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */
public class RequestStatsBean {
  private long totalSeconds;
  private int totalRequests;
  private double ratio;
  
  public RequestStatsBean() {}
  
  public RequestStatsBean(long totalSeconds, 
                          int totalRequests,
                          double ratio) {
    this.totalSeconds = totalSeconds;
    this.totalRequests = totalRequests;
    this.ratio = ratio;
  }
  
  public double getRatio() {
    return ratio;
  }
  public void setRatio(double ratio) {
    this.ratio = ratio;
  }
  public int getTotalRequests() {
    return totalRequests;
  }
  public void setTotalRequests(int totalRequests) {
    this.totalRequests = totalRequests;
  }
  public long getTotalSeconds() {
    return totalSeconds;
  }
  public void setTotalSeconds(long totalSeconds) {
    this.totalSeconds = totalSeconds;
  }
  
  

}
