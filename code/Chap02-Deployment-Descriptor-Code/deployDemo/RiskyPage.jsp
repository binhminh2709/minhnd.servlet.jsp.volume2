<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Risky JSP Page</TITLE>
</HEAD>
<BODY BGCOLOR="#FDF5E6">
  <H2>Risky Calculations</H2>
  <%@ page import="coreservlets.*"%>
  <%
    int n = ((int) (10 * Math.random()));
  %>
  <UL>
    <LI>n: <%=n%></LI>
    <LI>dangerousComputation(n): <%=DumbDeveloperException.dangerousComputation(n)%></LI>
  </UL>
</BODY>
</HTML>
