<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>JSP Init Test</TITLE>
</HEAD>
<BODY BGCOLOR="#FDF5E6">
  <H2>Init Parameters:</H2>
  <UL>
    <LI>First name: <%=firstName%></LI>
    <LI>Email address: <%=emailAddress%></LI>
  </UL>
</BODY>
</HTML>
<%!private String firstName = "First name is missing.";
  private String emailAddress = "Email address is missing";
  
  public void jspInit() {
    ServletConfig config = getServletConfig();
    if (config.getInitParameter("firstName") != null) {
      firstName = config.getInitParameter("firstName");
    }
    if (config.getInitParameter("emailAddress") != null) {
      emailAddress = config.getInitParameter("emailAddress");
    }
  }%>