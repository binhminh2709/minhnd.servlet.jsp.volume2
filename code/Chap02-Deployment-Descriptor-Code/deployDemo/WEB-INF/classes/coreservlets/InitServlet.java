package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Simple servlet used to illustrate servlet
 *  initialization parameters.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class InitServlet extends HttpServlet {
  private String firstName = "First name is missing.";
  private String emailAddress = "Email address is missing";

  public void init() {
    ServletConfig config = getServletConfig();
    if (config.getInitParameter("firstName") != null) {
      firstName = config.getInitParameter("firstName");
    }
    if (config.getInitParameter("emailAddress") != null) {
      emailAddress = config.getInitParameter("emailAddress");
    }
  }
  
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String uri = request.getRequestURI();
    out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
                "Transitional//EN\">" + "\n" +
                "<HTML>\n" + "<HEAD><TITLE>" + 
                "Init Servlet" + "</TITLE></HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H2>Init Parameters:</H2>\n" +
                "<UL>\n" +
                "<LI>First name: " + firstName + "\n" +
                "<LI>Email address: " + emailAddress + "\n" +
                "</UL>\n" + 
                "</BODY></HTML>");
  }
}
