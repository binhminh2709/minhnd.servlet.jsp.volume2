package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Simple servlet used to illustrate loading init-param 
 *  into ServletContext.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class LoadInitServlet extends HttpServlet {
  private String companyName = "Company name is missing";

  public void init() {
    ServletConfig config = getServletConfig();
    if (config.getInitParameter("companyName") != null) {
      companyName = config.getInitParameter("companyName");
    }
    ServletContext context = getServletContext();
    context.setAttribute("companyName", companyName);
  }
  
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " +
                "Transitional//EN\">" + "\n" +
                "<HTML>\n" + "<HEAD><TITLE>" + 
                "Load Init Servlet" + "</TITLE></HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H2>Init Parameter:</H2>\n" +
                "Company name: " + 
                getServletContext().getAttribute("companyName") +
                "\n" + "</BODY></HTML>");
  }
}
