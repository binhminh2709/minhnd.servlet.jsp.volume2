
package coreservlets;

import javax.servlet.*;
import java.util.*;

/** Simple listener that prints a report on the standard output 
 *  when the ServletContext is created or destroyed.
 */

public class ContextReporter implements ServletContextListener {
  
  public void contextInitialized(ServletContextEvent event) {
    System.out.println("Context created on " + new Date() + ".");
  }
  
  public void contextDestroyed(ServletContextEvent event) {
    System.out.println("Context destroyed on " + new Date() + ".");
  }
}
