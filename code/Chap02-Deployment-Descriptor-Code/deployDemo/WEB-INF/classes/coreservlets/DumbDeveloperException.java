package coreservlets;

/** Exception used to flag particularly onerous
    programmer blunders. Used to illustrate the
    exception-type web.xml element.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class DumbDeveloperException extends Exception {
  public DumbDeveloperException() {
    super("Duh. What was I *thinking*?");
  }

  public static int dangerousComputation(int n)
      throws DumbDeveloperException {
    if (n < 5) {
      return(n + 10);
    } else {
      throw(new DumbDeveloperException());
    }
  }
}
