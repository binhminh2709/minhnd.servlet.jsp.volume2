<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>404: Not Found</TITLE>
</HEAD>
<BODY BGCOLOR="#FDF5E6">
  <H2>Error!</H2>
  I'm sorry, but I cannot find a page that matches
  <%=request.getAttribute("javax.servlet.forward.request_uri")%>
  on the system. Maybe you should try one of the following:
  <UL>
    <LI>Go to the server's <A HREF="/">home page</A>.</LI>
    <LI>Search for relevant pages.<BR>
      <FORM ACTION="http://www.google.com/search">
        <CENTER>
          Keywords: <INPUT TYPE="TEXT" NAME="q"><BR> <INPUT TYPE="SUBMIT" VALUE="Search">
        </CENTER>
      </FORM>
    <LI>Admire a random multiple of 404: <%=404 * ((int) (1000 * Math.random()))%>.</LI>
    <LI>Try the amazing and amusing plinko.net <A HREF="http://www.plinko.net/404/">404 archive</A>.</LI>
  </UL>
</BODY>
</HTML>