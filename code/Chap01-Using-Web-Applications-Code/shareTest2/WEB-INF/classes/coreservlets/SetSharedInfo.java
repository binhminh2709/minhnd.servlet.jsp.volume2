package coreservlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Puts some data into the session, the servlet context, and
 *  two cookies. Then redirects the user to the servlet
 *  that displays info on sessions, the servlet context,
 *  and cookies.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages Volume II
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://volume2.coreservlets.com/.
 *  (C) 2007 Marty Hall, Larry Brown, and Yaakov Chaikin;
 *  may be freely used or adapted.
 */

public class SetSharedInfo extends HttpServlet {
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(true);
    session.setAttribute("sessionTest", "Session Entry One");
    ServletContext context = getServletContext();
    context.setAttribute("servletContextTest",
                         "Servlet Context Entry One");
    Cookie c1 = new Cookie("cookieTest1", "Cookie One");
    c1.setMaxAge(3600);     // One hour
    response.addCookie(c1); // Default path
    Cookie c2 = new Cookie("cookieTest2", "Cookie Two");
    c2.setMaxAge(3600);     // One hour
    c2.setPath("/");        // Explicit path: all URLs
    response.addCookie(c2);
    String url = request.getContextPath() +
                 "/showSharedInfo";
    // In case session tracking is based on URL rewriting.
    url = response.encodeRedirectURL(url);
    response.sendRedirect(url);
  }
}
