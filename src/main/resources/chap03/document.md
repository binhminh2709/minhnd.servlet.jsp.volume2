/** Declarative security * */

There are two major aspects to securing Web applications
1. Preventing unauthorized users from accessing sensitive data
2. Preventing attackers from stealing network data while it is in transit.


