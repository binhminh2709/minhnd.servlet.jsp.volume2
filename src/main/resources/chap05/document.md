/** SERVLET AND JSP FILTERS * */
public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException

The doFilter method is executed each time a filter is invoked (i.e., once for each request for a servlet or JSP page with which the filter is associated). It is this method that contains the bulk of the filtering logic.

The first argument is the ServletRequest associated with the incoming request. For simple filters, most of your filter logic is based on this object. Cast the object to HttpServletRequest if you are dealing with HTTP requests
and you need access to methods such as getHeader or getCookies that are unavailable in ServletRequest.

The second argument is the ServletResponse. In simple filters you often ignore this argument, but there are two cases when you use it.
- First, if you want to completely block access to the associated servlet or JSP page, you
can call response.getWriter and send a response directly to the client.
Section 5.7 (Blocking the Response) gives details;
Section 5.8 (Example: A Prohibited-Site Filter) gives an example.
- Second, if you want to modify the output of the associated servlet or JSP page, you can wrap the response inside an
object that collects all output sent to it. Then, after the servlet or JSP page is invoked, the filter can examine the output, modify it if appropriate, and then send it to the client.
Section 5.9 (Modifying the Response) for details.


