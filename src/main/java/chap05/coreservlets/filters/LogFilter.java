package chap05.coreservlets.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/** Simple filter that prints a report in the log file whenever the associated servlets or JSP pages are accessed.
*/
public class LogFilter implements Filter {
  
  protected FilterConfig config;
  private ServletContext context;
  private String filterName;

  @Override
  public void destroy() {
    
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    context.log(req.getRemoteHost() + " tried to access " + req.getRequestURL() + " on " + new Date() + ". " + "(Reported by " + filterName + ".)");
    chain.doFilter(request, response);
  }

  @Override
  public void init(FilterConfig config) throws ServletException {
    this.config = config; // In case it is needed by subclass.
    context = config.getServletContext();
    filterName = config.getFilterName();
  }
  
  /**
   * dir log: dir_tomcat\logs
   * */
}
