package chap06.coreservlets.listeners;

import java.util.*;
import javax.servlet.*;

/**
 * Listener that keeps track of the number of requests and the time each request
 * was made. This data is later used to calculate request frequency per second.
 * <P>
 * Taken from Core Servlets and JavaServer Pages Volume II from Prentice Hall
 * and Sun Microsystems Press, http://volume2.coreservlets.com/. (C) 2007 Marty
 * Hall, Larry Brown, and Yaakov Chaikin; may be freely used or adapted.
 */
public class RequestCounter implements ServletRequestListener {
  private ArrayList<Long> requestRecords = null;
  private static boolean countingFinished = false;
  
  public void requestInitialized(ServletRequestEvent requestEvent) {
    if (countingFinished) {
      return;
    }
    recordRequest(requestEvent, System.currentTimeMillis());
  }
  
  private void recordRequest(ServletRequestEvent requestEvent, Long time) {
    // Retrieve request records and record time
    ArrayList<Long> records = getRequestRecords(requestEvent);
    records.add(time);
  }
  
  private ArrayList<Long> getRequestRecords(ServletRequestEvent requestEvent) {
    // Check if it's already cached
    if (this.requestRecords != null) {
      return this.requestRecords;
    }
    
    // Initialize requestRecords and store it in ServletContext
    ServletContext context = requestEvent.getServletContext();
    requestRecords = new ArrayList<Long>();
    context.setAttribute("requestRecords", requestRecords);
    return requestRecords;
  }
  
  /**
   * Allow outside classes to stop collection of request statistics.
   */
  public static void setCountingFinished(boolean countingFinished) {
    RequestCounter.countingFinished = countingFinished;
  }
  
  public void requestDestroyed(ServletRequestEvent requestEvent) {
  }
}
