package chap06.coreservlets.tags;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import chap06.coreservlets.listeners.InitialCompanyNameListener;

/**
 * The InitialCompanyNameListener class has static methods that permit access to
 * the current and former company names. But, using these methods in JSP
 * requires explicit Java code, and creating beans that provided the information
 * would have yielded a cumbersome result. So, we simply move the code into a
 * custom tag.
 * <P>
 * Taken from Core Servlets and JavaServer Pages Volume II from Prentice Hall
 * and Sun Microsystems Press, http://volume2.coreservlets.com/. (C) 2007 Marty
 * Hall, Larry Brown, and Yaakov Chaikin; may be freely used or adapted.
 */

public class FormerCompanyNameTag extends SimpleTagSupport {
  private boolean useFullDescription = false;
  
  public void doTag() throws JspException, IOException {
    PageContext pageContext = (PageContext) getJspContext();
    ServletContext context = pageContext.getServletContext();
    String formerCompanyName = InitialCompanyNameListener.getFormerCompanyName(context);
    JspWriter out = pageContext.getOut();
    if (useFullDescription) {
      String formerCompanyDescription = "";
      if (!formerCompanyName.equals("")) {
        formerCompanyDescription = "(formerly " + formerCompanyName + ")";
      }
      out.print(formerCompanyDescription);
    } else {
      out.print(formerCompanyName);
    }
  }
  
  /**
   * If the user supplies a fullDescription attribute with the value "true"
   * (upper, lower, or mixed case), set the useFullDescription instance variable
   * to true. Otherwise, leave it false.
   */
  public void setFullDescription(String flag) {
    if (flag.equalsIgnoreCase("true")) {
      useFullDescription = true;
    }
  }
}
