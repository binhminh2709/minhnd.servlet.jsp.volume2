package chap06.coreservlets.tags;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import chap06.coreservlets.listeners.InitialCompanyNameListener;

/**
 * The InitialCompanyNameListener class has static methods that permit access to
 * the current and former company names. But, using these methods in JSP
 * requires explicit Java code, and creating beans that provided the information
 * would have yielded a cumbersome result. So, we simply move the code into a
 * custom tag.
 * <P>
 * Taken from Core Servlets and JavaServer Pages Volume II from Prentice Hall
 * and Sun Microsystems Press, http://volume2.coreservlets.com/. (C) 2007 Marty
 * Hall, Larry Brown, and Yaakov Chaikin; may be freely used or adapted.
 */

public class CompanyNameTag extends SimpleTagSupport {
  public void doTag() throws JspException, IOException {
    PageContext pageContext = (PageContext) getJspContext();
    ServletContext context = pageContext.getServletContext();
    String companyName = InitialCompanyNameListener.getCompanyName(context);
    JspWriter out = pageContext.getOut();
    out.print(companyName);
  }
}
